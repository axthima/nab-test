const _ = require('lodash');
const products = require('../datas/test-full-inte/products');


module.exports = {
  down: async () => {
    return Promise.resolve();
  },
  up: async (queryInterface) => {
    return Promise.all([
      queryInterface.bulkInsert(
        'products',
        _.map(products, (product, id) => ({ id, ...product })),
        {}
      )
    ]);
  }
};
