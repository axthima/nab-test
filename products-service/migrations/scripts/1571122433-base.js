module.exports = {
  down: async (queryInterface) => {
    return queryInterface.dropAllTables();
  },
  up: async (queryInterface, Sequelize) => {
    return (
      Promise.resolve()

        
        .then(() =>
          queryInterface.createTable('products', {
            id: {
              type: Sequelize.UUID,
              unique: true,
              primaryKey: true,
              isUUID: 4,
              defaultValue: Sequelize.UUIDV4
            },
            name: {
              type: Sequelize.STRING,
              allowNull: false,
            },
            description: {
              type: Sequelize.STRING,
              allowNull: false,
            },
            color: {
              type: Sequelize.STRING,
              allowNull: false,
              validate: {
                isIn: ['blue', 'yellow', 'red']
              }
            },
            price: {
              type: Sequelize.FLOAT,
              allowNull: false,
            },
          })
        )
    );
  }
};
