module.exports = {
  'af9fca0f-4668-4ae5-bf34-373506bba468': {
    name: 'product 1',
    description: 'description 1',
    color: 'blue',
    price: 1.1
  },
  'bf9fca0f-4668-4ae5-bf34-373506bba468': {
    name: 'product 2',
    description: 'description 2',
    color: 'red',
    price: 1.2
  },
  'cf9fca0f-4668-4ae5-bf34-373506bba468': {
    name: 'product 3',
    description: 'description 3',
    color: 'yellow',
    price: 1.3
  }
};
