# NAB

## About

This project uses [Feathers](http://feathersjs.com).  
It's better to not use the cli tools and stick to the supported scripts and commands
defined in `package.json`

## Getting Started

1. Make sure you have Node, npm, docker, and sequelize-cli installed and accessible globally.
2. Install your dependencies

```
npm install
```

3. Start postgres dababases

```
docker run --name nab-postgres -p 5433:5432 -e POSTGRES_PASSWORD=nab -e POSTGRES_USER=nab -d postgres

docker run --name nab-test-postgres -p 5434:5432 -e POSTGRES_PASSWORD=nab-test- -e POSTGRES_USER=nab-test -d postgres
```

4. Reset local/dev dababase

```
npm run reset-db-development
```

5. Start your app in dev mode

```
npm run dev
```

## Docs/Manual testing

Swagger docs are available if ENABLE_DOCS = true:

```
`/docs/?url=public.json`
```

## Testing

Full integration tests  
`npm run test-full-inte`


## Kubernetes liveness and readiness checks

This will be runnig if KUBERNETES_SERVICE_HOST environment variable is set.  
Port is defined in config files.  
This is using https://github.com/gajus/lightship

## Prometheus datas

Exposed on http://localhost:(promster port defined in conf).  
From what is exposed you can build more metrics using PromQL as described here https://github.com/tdeekens/promster#example-promql-queries

## Migrations/seeders workflow

The database is built from the migrations. We don't use sequelize.sync to ensure that what happen during development is the same as in production.   
Everytime a model is added/changed, the equivalent migrations must be created.  
Migrate should be run at every deployment. The last migration that ran is saved so that only new migrations will be run. 
However seeders need to be run manually when/if needed.  

## Run in production

1/ Build

no build needed, its using regular node syntax

2/ Run  
You need to set these environment variables

| Variable         | description                                                                                         |
| ---------------- | --------------------------------------------------------------------------------------------------- |
| `NODE_ENV`       | needs to be `production`                                                                            |
| `ENABLE_DOCS`    | if `true` it exposes the documentation. (suggestion : `true` except for production environment)     |
| `PORT`           | the port the node app is listening on                                                               |
| `HOST`           | the public hostname of the api (only used for display)                                              |
| `KUBERNETES_SERVICE_HOST` | set to true to enable Lightship                                             |
| `LIGHTSHIP_PORT` | the port the kubernetes health endpoints are exposed on                                             |
| `PROMSTER_PORT`  | the port the prometheus metrics are exposed on                                                      |
| `POSTGRES`       | the connection string to the postgres DB (ex : `postgres://....`) |                    |
| `OUTSIDE_HOST`    | the FE url           |

Then to run the app use  
`npm start`

