const toJSON = require('sequelize-json-schema');
const _ = require('lodash');

const doc = () => {
  const docTags = [];
  const docDefinitions = {};
  const docResources = {};

  return {
    getSwaggerDoc: () => ({
      swagger: '2.0',
      tags: docTags,
      paths: docResources,
      definitions: docDefinitions,
      schemes: ['http', 'https'],
      securityDefinitions: {
        Bearer: {
          type: 'apiKey',
          in: 'header',
          name: 'Authorization'
        }
      }
    }),
    addTag: ({ name, description }) => {
      docTags.push({
        name,
        description
      });
    },
    // TODO : TS definition for Model (from)
    addDefinition: ({
      name,
      from,
      exclude
    }) => {
      const jsonModel = toJSON(from);
      docDefinitions[name] = {
        type: jsonModel.type,
        properties: _.omit(jsonModel.properties, exclude || [])
      };
      docDefinitions[name + '_input'] = {
        type: jsonModel.type,
        properties: _.omit(jsonModel.properties, (exclude || []).concat('id'))
      };
    },
    addCustomResource: (path, resource) => {
      docResources[path] = resource;
    },
    addRestResource: ({
      customDataSchemas = {},
      path,
      tags,
      ref,
      idName,
      rootOnly,
      noRoot,
      excludes = {},
      findFilters = [],
      isAdmin = false
    }) => {
      const pathParamsRegexp = /{(.*?)}/gm;
      const pathParameters = [];

      let m = pathParamsRegexp.exec(path);
      while (m) {
        pathParameters.push({
          name: m[1],
          in: 'path',
          required: true,
          type: 'string'
        });
        m = pathParamsRegexp.exec(path);
      }

      const security = isAdmin ? [{ Admin: [] }] : [{ Bearer: [] }];

      if (!rootOnly) {
        docResources[`${path}/{${idName || 'id'}}`] = {
          get: !excludes.get ? {
            security,
            tags,
            produces: ['application/json'],
            parameters: [
              ...pathParameters,
              {
                in: 'path',
                name: idName || 'id',
                type: 'string',
                required: true
              }
            ],
            responses: {
              200: {
                schema: {
                  $ref: `#/definitions/${ref}`
                }
              }
            }
          } : undefined,
          delete: !excludes.delete ? {
            security,
            tags,
            produces: ['application/json'],
            parameters: [
              ...pathParameters,
              {
                in: 'path',
                name: idName || 'id',
                type: 'string',
                required: true
              }
            ],
            responses: {
              200: {
                schema: {
                  $ref: `#/definitions/${ref}`
                }
              }
            }
          } : undefined,
          put: !excludes.put ? {
            security,
            tags,
            consumes: ['application/json'],
            produces: ['application/json'],
            parameters: [
              ...pathParameters,
              {
                in: 'path',
                name: idName || 'id',
                type: 'string',
                required: true
              },
              {
                in: 'body',
                name: 'body',
                required: true,
                schema: {
                  $ref: `#/definitions/${ref}`
                }
              }
            ],
            responses: {
              200: {
                schema: {
                  $ref: `#/definitions/${ref}`
                }
              }
            }
          } : undefined,
          patch: !excludes.patch ? {
            security,
            tags,
            consumes: ['application/json'],
            produces: ['application/json'],
            parameters: [
              ...pathParameters,
              {
                in: 'path',
                name: idName || 'id',
                type: 'string',
                required: true
              },
              {
                in: 'body',
                name: 'body',
                required: true,
                schema: {
                  $ref: `#/definitions/${customDataSchemas.patch || ref}_input`
                }
              }
            ],
            responses: {
              200: {
                schema: {
                  $ref: `#/definitions/${ref}`
                }
              }
            }
          } : undefined
        };
      }

      if (!noRoot) {
        docResources[path] = {
          get: !excludes.list ? {
            security,
            tags,
            produces: ['application/json'],
            parameters: [
              ...pathParameters,
              {
                description: 'Number of results to return',
                in: 'query',
                name: '$limit',
                type: 'integer'
              },
              {
                description: 'Number of results to skip',
                in: 'query',
                name: '$skip',
                type: 'integer'
              },
              ..._.map(findFilters, key => ({
                in: 'query',
                name: key,
                type: 'string'
              }))
            ],
            responses: {
              200: {
                schema: {
                  type: 'object',
                  properties: {
                    total: {
                      type: 'integer',
                      format: 'int32'
                    },
                    limit: {
                      type: 'integer',
                      format: 'int32'
                    },
                    skip: {
                      type: 'integer',
                      format: 'int32'
                    },
                    data: {
                      type: 'array',
                      items: {
                        $ref: `#/definitions/${ref}`
                      }
                    }
                  }
                }
              }
            }
          } : undefined,
          post: !excludes.create ? {
            security,
            tags,
            consumes: ['application/json'],
            produces: ['application/json'],
            parameters: [
              ...pathParameters,
              {
                in: 'body',
                name: 'body',
                required: true,
                schema: {
                  $ref: `#/definitions/${customDataSchemas.create || ref}_input`
                }
              }
            ],
            responses: {
              200: {
                schema: {
                  $ref: `#/definitions/${ref}`
                }
              }
            }
          } : undefined
        };
      }
    }
  };
};

module.exports = {
  public: doc()
};
