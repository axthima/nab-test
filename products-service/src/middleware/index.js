const promster = require('@promster/express');

module.exports = function(app) {
  app.use(
    promster.createMiddleware({
      app,
      options: {
        accuracies: ['ms'],
        metricTypes: [
          'httpRequestsTotal',
          'httpRequestsSummary',
          'httpRequestsHistogram'
        ],
        labels: ['api'],
        getLabelValues: () => ({ api: 'toptal-reviews' })
      }
    })
  );
};
