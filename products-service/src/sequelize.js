const Sequelize = require('sequelize');
const _ = require('lodash');

module.exports = function(app) {
  const connectionString = app.get('postgres');
  const sequelize = new Sequelize(connectionString, {
    dialect: 'postgres',
    logging: false,
    define: {
      freezeTableName: true,
      timestamps: false
    }
  });

  // workaround https://github.com/sequelize/sequelize/issues/6148
  Sequelize.Model.count = _.wrap(Sequelize.Model.count, function count(_count, options) {
    return _count.bind(this)(options).then(r => options.group ? r.length : r);
  });

  const oldSetup = app.setup;

  app.set('sequelizeClient', sequelize);

  app.setup = function(...args) {
    const result = oldSetup.call(this, ...args);

    // Set up data relationships
    const models = sequelize.models;
    Object.keys(models).forEach(name => {
      if ('associate' in models[name]) {
        models[name].associate(models);
      }
    });

    return result;
  };
};
