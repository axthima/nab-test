const { createLogger, format, transports } = require('winston');

// Configure the Winston logger. For the complete documentation seee https://github.com/winstonjs/winston
const moduleExports = createLogger({
  level: 'info',
  format: format.combine(format.splat(), format.simple()),
  transports: [
    new transports.Console({
      silent: process.env.NODE_ENV === 'test-full-inte'
    })
  ]
});

module.exports = moduleExports;
