const { Sequelize } = require('sequelize');

module.exports = function(app) {
  const sequelizeClient = app.get('sequelizeClient');

  const products = sequelizeClient.define(
    'products',
    {
      id: {
        type: Sequelize.UUID,
        unique: true,
        primaryKey: true,
        isUUID: 4,
        defaultValue: Sequelize.UUIDV4
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      description: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      color: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          isIn: ['blue', 'yellow', 'red']
        }
      },
      price: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  products.associate = () => {
    // model associations
  };

  return products;
};
