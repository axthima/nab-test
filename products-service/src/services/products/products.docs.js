const docs = require('../../docs');
const _ = require('lodash');

module.exports = (model) => {
  const restExcludes = {
    public: { put: true, create: true, patch: true, delete: true },
  };

  const findFilters = {
    public: ['$sort[price]', '$sort[name]', 'color']
  };

  _.each(docs, (doc, key) => {
    doc.addDefinition({
      name: 'Products',
      from: model
    });
    doc.addRestResource({
      path: '/products',
      tags: ['products'],
      ref: 'Products',
      excludes: restExcludes[key],
      findFilters: findFilters[key]
    });
  });
};
