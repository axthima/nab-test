const createService = require('feathers-sequelize');
const createModel = require('../../models/products.model');
const productsHooks = require('./products.hooks');
const docs = require('./products.docs');
const hideFromOutside = require('../../hooks/hideFromOutside');

module.exports = (app) => {
  const model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model: model,
    paginate
  };

  const sequelizeService = createService(options);

  app.use('/products', sequelizeService);
  app.service('products').hooks(productsHooks);

  app.use('/_internal/products', sequelizeService);
  app.service('_internal/products').hooks(hideFromOutside);

  docs(model);
};
