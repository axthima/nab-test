const docs = require('../docs');
const restaurants = require('./products/products.service.js');
const _ = require('lodash');

module.exports = function(app) {
  app.configure(restaurants);

  _.each(docs, doc => {
    doc.addTag({
      name: 'products',
      description: 'Operations available on products'
    });
  });
};
