const cors = require('cors');
const helmet = require('helmet');
const logger = require('./logger');

const _ = require('lodash');

const feathers = require('@feathersjs/feathers');
const configuration = require('@feathersjs/configuration');
const express = require('@feathersjs/express');

const middleware = require('./middleware');
const services = require('./services');
const appHooks = require('./app.hooks');
const docs = require('./docs');

const sequelize = require('./sequelize');


const app = express(feathers());

// Load app configuration
app.configure(configuration());

// Enable security, CORS, compression, favicon and body parsing
app.use(helmet());
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Set up Plugins and providers
app.configure(express.rest());
// Configure database adapters
app.configure(sequelize);
//

// Configure other middleware (see `middleware/index.ts`)
app.configure(middleware);
// Set up our services (see `services/index.ts`)
app.configure(services);

app.get('/', (req, res) => {
  res.json({ name: 'nab' });
});

if (process.env.ENABLE_DOCS === 'true') {
  _.each(docs, (doc, key) =>
    app.get(`/docs/${key}.json`, (req, res) => {
      res.json(doc.getSwaggerDoc());
    })
  );
}

// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(express.errorHandler({ logger }));

app.hooks(appHooks);

module.exports = app;
