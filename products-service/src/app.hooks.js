const log = require('./hooks/log');
const _ = require('lodash');

const hooks = {
  before: {
    all: [log()],
    create: [],
    find: [],
    get: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [log()],
    create: [],
    find: [],
    get: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [
      log(),
      // remove eventual leaks of informations
      context => {
        _.each(context.error.errors, error => {
          delete error.instance;
        });
        return context;
      }
    ],
    create: [],
    find: [],
    get: [],
    update: [],
    patch: [],
    remove: []
  },
  finally: {}
};

module.exports = hooks;
