const commonHooks = require('feathers-hooks-common');
const errors = require('@feathersjs/errors');

module.exports = {
  before: {
    all: commonHooks.unless(commonHooks.isProvider('server'), () => {
      throw new errors.NotFound();
    })
  }
};
