const { Forbidden } = require('@feathersjs/errors');

module.exports = (why = 'Forbidden') => {
  return () => {
    throw new Forbidden(why);
  };
};
