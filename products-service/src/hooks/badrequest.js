const { BadRequest } = require('@feathersjs/errors');

module.exports = (why = 'BadRequest') => {
  return () => {
    throw new BadRequest(why);
  };
};
