const logger = require('./logger');
const app = require('./app');
const { createLightship } = require('lightship');
const promster = require('@promster/server');
const promsterExpress = require('@promster/express');

const port = app.get('port');
const server = app.listen(port);

const lightship = createLightship({
  port: app.get('lightship').port
});

lightship.registerShutdownHandler(() => {
  server.close();
});

promster
  .createServer({ port: app.get('promster').port })
  .then(() =>
    logger.info('Promster running on port %d', app.get('promster').port)
  );

process.on('unhandledRejection', (reason, p) => {
  logger.error('Unhandled Rejection at: Promise ', p, reason);
});

server.on('listening', () => {
  logger.info(
    'Feathers application started on http://%s:%d',
    app.get('host'),
    port
  );
  lightship.signalReady();
  logger.info('Lightship running on port %d', app.get('lightship').port);
  promsterExpress.signalIsUp();
  return app.get('sequelizeClient');
});
