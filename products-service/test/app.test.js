const rp = require('request-promise');
const utils = require('./utils');

describe('Feathers application tests', () => {
  beforeAll(utils.testStart);
  afterAll(utils.testStop);

  it('should be listening and responding', () => {
    expect.assertions(1);
    return rp({
      url: utils.getUrl('/'),
      json: true
    }).then(body => expect(body.name).toEqual('nab'));
  });

  describe('404', () => {
    it('shows a 404 JSON error without stack trace', () => {
      expect.assertions(4);
      return rp({
        url: utils.getUrl('path/to/nowhere'),
        json: true
      }).catch(res => {
        expect(res.statusCode).toBe(404);
        expect(res.error.code).toBe(404);
        expect(res.error.message).toBe('Page not found');
        expect(res.error.name).toBe('NotFound');
      });
    });
  });
});
