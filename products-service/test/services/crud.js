const rp = require('request-promise');
const utils = require('../utils');

let token = '';

module.exports = {
  resetToken: (tokenNew = '') => {
    token = tokenNew;
  },
  getToken: () => {
    return token;
  },
  authenticate: (credentials) => {
    token = '';
    return rp({
      url: utils.getUrl('authentication'),
      json: true,
      method: 'POST',
      body: credentials
    })
      .then(r => {
        token = r.accessToken;
        return r;
      });
  },
  verification: (verif) =>
    rp({
      url: utils.getUrl('verification'),
      json: true,
      method: 'POST',
      body: verif,
      headers: {
        Authorization: token ? `Bearer ${token}` : undefined
      },
    }),
  sendVerificationEmail: (data) =>
    rp({
      url: utils.getUrl('send-verification-email'),
      json: true,
      method: 'POST',
      body: data,
      headers: {
        Authorization: token ? `Bearer ${token}` : undefined
      },
    }),
  forgotPassword: (verif) =>
    rp({
      url: utils.getUrl('forgot-password'),
      json: true,
      method: 'POST',
      body: verif,
      headers: {
        Authorization: token ? `Bearer ${token}` : undefined
      },
    }),
  resetForgotPassword: (data) =>
    rp({
      url: utils.getUrl('reset-forgot-password'),
      json: true,
      method: 'POST',
      body: data,
      headers: {
        Authorization: token ? `Bearer ${token}` : undefined
      },
    }),
  resetEmail: (reset) =>
    rp({
      url: utils.getUrl('reset-email'),
      json: true,
      method: 'POST',
      body: reset,
      headers: {
        Authorization: token ? `Bearer ${token}` : undefined
      },
    }),
  acceptResetEmail: (data) =>
    rp({
      url: utils.getUrl('accept-reset-email'),
      json: true,
      method: 'POST',
      body: data,
      headers: {
        Authorization: token ? `Bearer ${token}` : undefined
      },
    }),
  unblockUser: (data) =>
    rp({
      url: utils.getUrl('unblock-user'),
      json: true,
      method: 'POST',
      body: data,
      headers: {
        Authorization: token ? `Bearer ${token}` : undefined
      },
    }),
  resetPassword: (data) =>
    rp({
      url: utils.getUrl('reset-password'),
      json: true,
      method: 'POST',
      body: data,
      headers: {
        Authorization: token ? `Bearer ${token}` : undefined
      },
    }),
  resetUserPassword: (data) =>
    rp({
      url: utils.getUrl('reset-user-password'),
      json: true,
      method: 'POST',
      body: data,
      headers: {
        Authorization: token ? `Bearer ${token}` : undefined
      },
    }),
  resetUserEmail: (data) =>
    rp({
      url: utils.getUrl('reset-user-email'),
      json: true,
      method: 'POST',
      body: data,
      headers: {
        Authorization: token ? `Bearer ${token}` : undefined
      },
    }),
  list: ({ path, headers = {}, qs = {} }) =>
    rp({
      url: utils.getUrl(path),
      json: true,
      headers: {
        Authorization: token ? `Bearer ${token}` : undefined,
        ...headers
      },
      method: 'GET',
      qs
    }).catch(res => {
      throw res.statusCode;
    }),
  create: ({ path, object }) =>
    rp({
      url: utils.getUrl(path),
      json: true,
      headers: {
        Authorization: token ? `Bearer ${token}` : undefined
      },
      method: 'POST',
      body: object
    }).catch(res => {
      throw res.statusCode;
    }),
  get: ({ path, id }) =>
    rp({
      url: utils.getUrl(`${path}/${id}`),
      json: true,
      headers: {
        Authorization: token ? `Bearer ${token}` : undefined
      },
      method: 'GET'
    }).catch(res => {
      throw res.statusCode;
    }),
  patch: ({ path, id, object }) =>
    rp({
      url: utils.getUrl(`${path}/${id}`),
      json: true,
      headers: {
        Authorization: token ? `Bearer ${token}` : undefined
      },
      method: 'PATCH',
      body: object
    }).catch(res => {
      throw res.statusCode;
    }),
  put: ({ path, id, object }) =>
    rp({
      url: utils.getUrl(`${path}/${id}`),
      json: true,
      headers: {
        Authorization: token ? `Bearer ${token}` : undefined
      },
      method: 'PUT',
      body: object
    }).catch(res => {
      throw res.statusCode;
    }),
  remove: ({ path, id }) =>
    rp({
      url: utils.getUrl(`${path}/${id}`),
      json: true,
      headers: {
        Authorization: token ? `Bearer ${token}` : undefined
      },
      method: 'DELETE'
    }).catch(res => {
      throw res.statusCode;
    })
};
