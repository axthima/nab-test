const utils = require('../utils');
const CRUD = require('./crud');
const child_process = require('child_process');

const productId = 'af9fca0f-4668-4ae5-bf34-373506bba468';

describe('products service', () => {
  beforeAll(utils.testStart);
  afterAll(utils.testStop);

  describe('testing products model as public ', () => {
    beforeAll(() => {
      child_process.execSync('npm run reset-db-test-full-inte');

      CRUD.resetToken();
    });

    it('should not be able to create', () => {
      expect.assertions(1);
      return CRUD.create({
        path: '/products',
        object: {
          name: 'product 4',
          description: 'description 4',
          color: 'red',
          price: 1.4
        }
      }).catch((code) => expect(code).toStrictEqual(405));
    });

    it('should allow viewing a restaurant', () => {
      expect.assertions(1);
      return CRUD.get({
        path: '/products',
        id: productId
      }).then((r) => {
        expect(r).toStrictEqual({
          id: productId,
          name: 'product 1',
          description: 'description 1',
          color: 'blue',
          price: 1.1
        });
      });
    });

    it('should forbid PUT', () => {
      expect.assertions(1);
      return CRUD.put({
        path: '/products',
        id: productId,
        object: {}
      }).catch((code) => expect(code).toStrictEqual(405));
    });

    it('should allow GET list', () => {
      expect.assertions(1);
      return CRUD.list({
        path: '/products'
      }).then((r) => {
        expect(r.total).toStrictEqual(3);
      });
    });

    it('should forbid PATCH', () => {
      expect.assertions(1);
      return CRUD.patch({
        path: '/products',
        id: productId,
        object: {}
      }).catch((code) => expect(code).toStrictEqual(405));
    });

    it('should forbid DELETE', () => {
      expect.assertions(1);
      return CRUD.remove({
        path: '/products',
        id: productId
      }).catch((code) => expect(code).toStrictEqual(405));
    });
  });
});
