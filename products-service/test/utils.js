const url = require('url');
const app = require('../src/app');

let testServer;

module.exports = {
  testStart: (done) => {
    testServer = app.listen(app.get('port'));
    testServer.once('listening', () => done());
  },
  testStop: (done) => {
    testServer.close(() => {
      app
        .get('sequelizeClient')
        .close()
        .then(() => done());
    });
  },
  getUrl: (pathname) =>
    url.format({
      hostname: app.get('host') || 'localhost',
      protocol: 'http',
      port: app.get('port'),
      pathname
    }),
  app,
  getInternal: (model) => {
    return app.service(`_internal/${model}`);
  }
};
