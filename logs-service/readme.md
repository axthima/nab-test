SETUP
its a serverless app so you just need to have serverless installed and install dependencies:
```
npm install -g serverless
npm install
```

RUN LOCAL API
```
sls offline
```

RUN TESTS
```
npm run test
```