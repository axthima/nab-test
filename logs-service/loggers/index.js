const _ = require('lodash');

const loggers = [require('./requestbin') /*, logger2 .... */];

module.exports = (event, data) => {
  _.each(loggers, logger => logger(event, data));
};
