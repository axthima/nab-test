const fetch = require("node-fetch");

module.exports = (event, data) => {
	const options = {
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    mode: "cors",
    body: JSON.stringify(data),
  }

  fetch(`https://${process.env.requestbin}.x.pipedream.net/${event}`, options);
}
