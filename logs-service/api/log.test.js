const loggers = require('../loggers');
const fetch = require("node-fetch");
const log = require('./log');

jest.mock('node-fetch');

jest.mock('../loggers');
loggers.mockImplementation(() => {});

process.env.products = 'http://product.api';

test('log search', async () => {
  const res = await log.search({ body: '{"search": "yeah"}'});

  expect(res.statusCode).toBe(200);
  expect(loggers).toHaveBeenCalledWith('search', {search: 'yeah'});
});

test('log view', async () => {
  fetch.mockResolvedValue({
    json: () => Promise.resolve({ name: 'product' })
  });

  const res = await log.view({ body: '{"id": "productid"}'});

  expect(res.statusCode).toBe(200);
  expect(loggers).toHaveBeenCalledWith('view', {id: 'productid', product: { name: 'product' }});
  expect(fetch).toHaveBeenCalledWith(`${process.env.products}/products/productid`);
});
