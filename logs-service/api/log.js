'use strict';

const fetch = require("node-fetch");
const _ = require('lodash');
const loggers = require('../loggers');

module.exports.search = async (event, context) => {
  let data;

  try {
    data = JSON.parse(event.body);
  } catch(e) {
    return {
      statusCode: 400,
    };
  }

  loggers('search', data);

  return {
    statusCode: 200,
    body: JSON.stringify(data),
  };
};

module.exports.view = async (event, context) => {
  let data;

  try {
    data = JSON.parse(event.body);
  } catch(e) {
    return {
      statusCode: 400,
    };
  }

  const res = await fetch(`${process.env.products}/products/${data.id}`);
  data.product = await res.json();

  loggers('view', data);

  return {
    statusCode: 200,
    body: JSON.stringify(data),
  };
};

module.exports.docs = async (event, context) => {
  return {
    statusCode: 200,
    body: JSON.stringify(require('./log.docs.json')),
  };
};
