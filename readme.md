# NAB

## Architecture

Please refer to the pdf that is in the repo.

## Get started

Follow the readme in each directory to launch a local products-services and logs-services. You can also run tests.

## Play with the services

You can access the documentation and try the endpoints with Swagger.
The logs service is intented to be called from the apps.

Products service
```
https://petstore.swagger.io/?url=http://localhost:3030/docs/public.json
```

Logs service.
The log service is intented to be called from the Frontend since it only tracks client side events (search, view product).
```
https://petstore.swagger.io/?url=http://localhost:3000/dev/docs.json
```

Currently the Logs Service only outputs logs to requestbin.
You can follow in realtime what is being logged.
The view event only takes a product id as input but will hydrate the product infos from the products service before being logged.
```
https://requestbin.com/r/enie9smq8or7c/
```